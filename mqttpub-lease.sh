#!/bin/sh
set -x
gw_dev=`route -n |grep ^0 |awk '{print $8}'`
ip=`ifconfig ${gw_dev} |grep "inet "|awk '{print $2}'`


mqtt_server=$ip
nfs_server=$mqtt_server
api_server=k8s-api.globelock.home
proxy_host=$mqtt_server
op="${1:-op}"
# mac="${2:-mac}"
ip="${3:-ip}"
mac=`arp ${ip} | tail -n 1 |awk '{print $3}'`
arp_mac=`echo $mac  |sed 's/:/-/g'`
hostname=""
arp_mac=`arp ${ip} | tail -n 1 |awk '{print $3}'  |sed 's/:/-/g'`

tstamp="`date '+%b-%d-%Y %H:%M:%S'`"
server_configs="/NFS/boot_scripts/server_configs"

topic="network/dhcplease"
payload="${op} ${mac} ${ip} ${tstamp} (Booting) {$@}"
# export > /tmp/tftp-export
# echo >> /tmp/tftp-export
echo $@ > /tmp/tftp-export
base_mount="/NFS/boot_scripts"

boot_lower="/NFS/p1/boot"
boot_upper="/NFS/boot_overlay/upper/${arp_mac}"
boot_work="/NFS/boot_overlay/work/${arp_mac}"
boot_mount="/NFS/tftpboot/${arp_mac}"
work_lower="/NFS/p1/"
work_upper="/NFS/work_overlay/upper/${arp_mac}"
work_work="/NFS/work_overlay/work/${arp_mac}"
work_mount="/NFS/work_overlay/drive/${arp_mac}"
# docker_lower="/NFS/docker_overlay/lower"
# docker_upper="/NFS/docker_overlay/upper/${arp_mac}"
# docker_work="/NFS/docker_overlay/work/${arp_mac}"
docker_mount="/NFS/docker_mounts/${arp_mac}"
kubelet_mount="/NFS/kubelet_mounts/${arp_mac}"


if [ "${op}" = "arp-add" ]
then
	for dir in `echo $boot_upper $boot_lower $boot_work $boot_mount $work_upper $work_work $work_mount $docker_mount $kubelet_mount	`
	do
		if [ ! -d  $dir ]
		then
			mosquitto_pub -h ${mqtt_server} -t "${topic}/${arp_mac}/log" -m "mkdir $dir"
			mkdir  -pv $dir
		fi
	done
		if [ `mount |grep -c ${boot_mount}` -eq 0 ]
	then
		mount -t overlay ${arp_mac}-boot -o lowerdir=${boot_lower},upperdir=${boot_upper},workdir=${boot_work},nfs_export=on,index=on,redirect_dir=nofollow ${boot_mount}
		echo "console=serial0,115200 console=tty1 root=/dev/nfs nfsroot=192.168.1.80:${work_mount},vers=4.2 rw ip=::::${arp_mac}::dhcp elevator=deadline rootwait cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1" > ${boot_mount}/cmdline.txt
	fi
fi

if [ `grep ${base_mount}  /etc/exports | grep -v ^\# -c` -ne 1 ]
then
	cat << EOF >> /etc/exports 
${base_mount} *(rw,sync,no_subtree_check,no_root_squash,nohide,crossmnt) 
EOF
	nfs_reload=1
fi

if [ ! -f "/NFS/tftpboot/start4.elf" ]
then 
	mkdir /NFS/tftpboot
	cp ${boot_lower}/start4.elf  /NFS/tftpboot/
fi

mosquitto_pub -h "${mqtt_server}"  -t "${topic}" -m "ALL:    ${payload}" -r
if [ "${op}" = "tftp" ]
then
	
	if [ ! $tftp_boot ]
	then
		mosquitto_pub -h ${mqtt_server} -t "nodes/provision/${arp_mac}" -m "booting" -r
		mkdir  -p /NFS/tftpboot/${arp_mac}
		# setup and mount overlay
		#  mount -t overlay overlay -o lowerdir=e4-5f-01-67-b8-f9,upperdir=foo,workdir=foo-work foo2
		

		for dir in `echo $boot_upper $boot_lower $boot_work $boot_mount $work_upper $work_work $work_mount $docker_mount $kubelet_mount	`
		do
			if [ ! -d  $dir ]
			then
				mosquitto_pub -h ${mqtt_server} -t "${topic}/${arp_mac}/log" -m "mkdir $dir"
				mkdir  -pv $dir
			# else	
			# 	mosquitto_pub -h ${mqtt_server} -t "${topic}/${arp_mac}/log" -m "$dir exists"		
			fi
		done

		if [ `mount |grep -c ${boot_mount}` -eq 0 ]
		then
			# mkdir $boot_upper $boot_lower $boot_work $boot_mount $work_upper $work_work $work_mount $docker_mount $kubelet_mount -p
			# mkdir $boot_upper $boot_lower $boot_work $boot_mount $work_upper $work_work $work_mount $docker_upper $docker_work $docker_mount -p
			mount -t overlay ${arp_mac}-boot -o lowerdir=${boot_lower},upperdir=${boot_upper},workdir=${boot_work},nfs_export=on,index=on,redirect_dir=nofollow ${boot_mount}
			echo "console=serial0,115200 console=tty1 root=/dev/nfs nfsroot=192.168.1.80:${work_mount},vers=3 rw ip=::::${arp_mac}::dhcp elevator=deadline rootwait cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1" > ${boot_mount}/cmdline.txt
			# echo "console=serial0,115200 console=tty1 root=/dev/nfs nfsroot=192.168.1.80:/${work_mount},vers=3 rw ip=::::${arp_mac}::dhcp elevator=deadline rootwait " > ${boot_mount}/cmdline.txt
			# cp /NFS/p1/boot/*  /NFS/tftpboot/${arp_mac}/
		fi
		if [ `mount |grep -c ${work_mount}` -eq 0 ]
		then
		
			mount -t overlay ${arp_mac}-work -o lowerdir=${work_lower},upperdir=${work_upper},workdir=${work_work},nfs_export=on,index=on,redirect_dir=nofollow ${work_mount}

			echo $arp_mac > ${work_mount}/etc/hostname
		fi
		# Setup NFS export
		if [ `grep ${work_mount}  /etc/exports | grep -v ^\# -c` -ne 1 ]
		then
			cat << EOF >> /etc/exports 
${work_mount} ${ip}(fsid=1,rw,sync,no_subtree_check,no_root_squash,nohide,crossmnt) 
EOF
			nfs_reload=1
		fi	
		# if [ ! -f ${docker_mount} ]
		# then
		
		# 	rm ${docker_mount} -fr
		# fi
		# Setup NFS export
		if [ `grep ${docker_mount}  /etc/exports | grep -v ^\# -c` -ne 1 ]
		then
			cat << EOF >> /etc/exports 
${docker_mount} ${ip}(fsid=2,rw,sync,no_subtree_check,no_root_squash,nohide,crossmnt) 
EOF
			nfs_reload=1
		fi	
	fi
	mosquitto_pub -h ${mqtt_server} -t "${topic}/${arp_mac}" -m "${payload}" -r
	tftp_boot=1
fi

if [ $tftp_boot ]
then
	# add other directorys for node to exports for NFS
	if [ `grep ${kubelet_mount}  /etc/exports | grep -v ^\# -c` -ne 1 ]
	then
		cat << EOF >> /etc/exports 
${kubelet_mount} ${ip}(fsid=3,rw,sync,no_subtree_check,no_root_squash,nohide,crossmnt) 
EOF
		nfs_reload=1
	fi
	if [ `grep ${boot_mount}  /etc/exports | grep -v ^\# -c` -ne 1 ]
	then
		cat << EOF >> /etc/exports 
${boot_mount} ${ip}(fsid=4,rw,sync,no_subtree_check,no_root_squash,nohide,crossmnt) 
EOF
		nfs_reload=1
	fi
	echo mqtt_server=${mqtt_server} > ${server_configs}
	echo nfs_server=${nfs_server} >> ${server_configs}
	echo api_server=${api_server} >> ${server_configs}
	echo proxy_host=${api_server} >> ${server_configs}
	
	# if [ ! -d /NFS/docker/${arp_mac} ]
	# then
	# 	mkdir /NFS/docker/${arp_mac} -p
	# fi
	if [ ! -d /NFS/kubelet/${arp_mac} ]
	then
		mkdir /NFS/kubelet/${arp_mac} -p
	fi

	cat << EOF > ${work_mount}/etc/fstab
proc /proc proc defaults 0 0
# 192.168.1.80:/${work_mount} / nfs noatime 0 0
${nfs_server}:/NFS/boot_scripts /boot_scripts nfs4 rw,soft 0 0
${nfs_server}:${docker_mount} /var/lib/docker nfs4 rw,soft 0 0
${nfs_server}:${kubelet_mount} /var/lib/kubelet nfs4 rw,soft 0 0
${nfs_server}:${boot_mount} /boot nfs4 defaults 0 0
EOF
	if [ ! -d ${work_mount}/var/lib/docker ]
	then
		# image directory for docker mount
		mkdir -p ${work_mount}/var/lib/docker
		nfs_reload=1
	fi
	if [ $nfs_reload ]
	then
#		service nfs-server reload
		exportfs -r
	fi	
fi
