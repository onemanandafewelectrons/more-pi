VERSION := 1.0.7
RPM := jlcox1970/fpm-in-docker:1.13.1 fpm -f -m 'omafe@fpm-rpmbuilder'  -s dir -t rpm --rpm-dist el8 -a aarch64 -n tftpboot-server 
DEB := jlcox1970/fpm-in-docker:1.13.1 fpm -f -m 'omafe@fpm-debbuilder'  -s dir -t deb -a aarch64 -n tftpboot-server 

SCRIPTS := --after-upgrade tftp-server/install_scripts/after-install --after-install tftp-server/install_scripts/after-install --before-remove tftp-server/install_scripts/before_remove
TFTP_SERVER := ./tftp-server/tftpboot-server=/usr/local/bin/tftpboot-server ./tftp-server/systemd/tftpboot-server.service=/etc/systemd/system/tftpboot-server.service
RSYSLOG := ./tftp-server/rsyslog/tftpboot-server.conf=/etc/rsyslog.d/tftpboot-server.conf
LOGROTATE := ./tftp-server/logrotate/tftpboot-server.conf=/etc/logrotate.d/tftpboot-server.conf

all: build-rpm build-deb

build-rpm: build-bin
	setenforce 0	
	docker run -ti --rm -v ${PWD}:/code -w /code $(RPM)  -v $(VERSION) $(TFTP_SERVER) $(RSYSLOG) $(LOGROTATE)
	setenforce 1
build-deb: build-bin
	setenforce 0	
	docker run -ti --rm -v ${PWD}:/code -w /code $(DEB)  -v $(VERSION) $(TFTP_SERVER) $(RSYSLOG) $(LOGROTATE)
	setenforce 1

build-bin:
	make -C tftp-server
	chmod +x tftp-server/tftpboot-server	

install: build-rpm
	yum install -y tftpboot-server-$(VERSION)-1.el8.aarch64.rpm

remove:
	yum remove -y tftpboot-server-$(VERSION)-1.el8.aarch64