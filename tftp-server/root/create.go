package root

import (
	"fmt"
	"onemanandafewelectrons/more-pi/overlay"
	"os"
)

var NFS_SERVER string

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func create_dir(dir string) {
	_, err := os.ReadDir(dir)
	if err != nil {
		fmt.Println("dir not exist " + dir)
		check(os.MkdirAll(dir, 0755))
	}
}

func Create(tftp_hostname string, ROOT_BASE string, ROOT_SRC string) {

	client_path := ROOT_BASE + tftp_hostname + "/"
	upper_dir := client_path + "upper"
	work_dir := client_path + "work"
	mount_dir := client_path + "mount"
	create_dir(upper_dir)
	create_dir(work_dir)
	create_dir(mount_dir)
	if overlay.Check(mount_dir) != 1 {
		mount_options := "lowerdir=" + ROOT_SRC + ",upperdir=" + upper_dir + ",workdir=" + work_dir + ",nfs_export=on,index=on,redirect_dir=nofollow"
		overlay.Mount(tftp_hostname+"_root", mount_dir, mount_options)
	}
}
