package overlay

import (
	"fmt"
	"os/exec"
)

func Mount(mountpoint string, device string, options string) {
	cmd_line := "mount -t overlay " + mountpoint + " -o " + options + " " + device
	cmd, _ := exec.Command("/bin/bash", "-c", cmd_line).Output()
	fmt.Println(cmd_line)
	fmt.Println(cmd)
}
