package overlay

import (
	"fmt"

	"github.com/moby/sys/mountinfo"
)

func Check(mountpoint string) int {
	mounted, err := mountinfo.Mounted(mountpoint)
	if err != nil {
		fmt.Println(err)
	}
	if mounted {
		return 1
	}
	return 0
}
