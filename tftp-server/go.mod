module onemanandafewelectrons/more-pi

go 1.16

require (
	github.com/moby/sys/mountinfo v0.6.1 // indirect
	github.com/mostlygeek/arp v0.0.0-20170424181311-541a2129847a // indirect
	github.com/pin/tftp v0.0.0-20210809155059-0161c5dd2e96 // indirect
	k8s.io/mount-utils v0.23.6 // indirect
)
