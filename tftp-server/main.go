package main

import (
	"fmt"
	"io"
	"log"
	"onemanandafewelectrons/more-pi/boot"
	"onemanandafewelectrons/more-pi/custom_files"
	"onemanandafewelectrons/more-pi/nfs"
	"onemanandafewelectrons/more-pi/root"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/mostlygeek/arp"
	"github.com/pin/tftp"
)

var NFS_ROOT = "/NFS"
var TFTP_SRC_DIR = NFS_ROOT + "/p1/boot/"  // location of boot src for overlay
var ROOT_SRC_BASE = NFS_ROOT + "/p1/root/" // locate of root parition base for overlay

var TFTP_BASE = NFS_ROOT + "/tftpboot/"
var ROOT_BASE = NFS_ROOT + "/clients/"

var BOOT_SCRIPTS = NFS_ROOT + "/boot_scripts"
var DOCKER = NFS_ROOT + "/docker"
var KUBELET = NFS_ROOT + "/kubelet"

var NFS_SERVER string
var mutex = &sync.Mutex{}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func create_dir(dir string) {
	_, err := os.ReadDir(dir)
	if err != nil {
		fmt.Println("dir not exist " + dir)
		check(os.MkdirAll(dir, 0644))
	}
}
func readHandler(filename string, rf io.ReaderFrom) error {
	// mutex.Lock()
	// defer mutex.Unlock()
        fmt.Println("in handler")
	raddr := rf.(tftp.OutgoingTransfer).RemoteAddr().IP
	laddr := rf.(tftp.RequestPacketInfo).LocalIP()
	NFS_SERVER = laddr.String()
	mac := arp.Search(raddr.String())
	tftp_hostname := strings.ReplaceAll(string(mac), ":", "-")
	log.Println("RRQ from", raddr.String(), "To ", laddr.String(), "SourceMAC :", mac, " Host name : ", tftp_hostname, " Filename :"+filename)
	root_fs := ROOT_BASE + tftp_hostname

	// Split file name on / and if len=2 then take first to build out the boot disk
	boot_mount := boot.Create(tftp_hostname, TFTP_BASE, TFTP_SRC_DIR, filename, root_fs+"/mount", NFS_SERVER)
	root.Create(tftp_hostname, ROOT_BASE, ROOT_SRC_BASE)
	docker_mount := DOCKER + "/" + tftp_hostname
	kubelet_mount := KUBELET + "/" + tftp_hostname
	create_dir(docker_mount)
	create_dir(kubelet_mount)
	nfs.AddMount(root_fs+"/mount", raddr.String(), "1")
	nfs.AddMount(boot_mount, raddr.String(), "2")
	nfs.AddMount(docker_mount, raddr.String(), "3")
	nfs.AddMount(kubelet_mount, raddr.String(), "4")
	nfs.AddMount(BOOT_SCRIPTS, raddr.String(), "5")

	// custom files for node
	custom_files.CreateFSTAB(root_fs+"/mount", docker_mount, kubelet_mount, boot_mount, NFS_SERVER, BOOT_SCRIPTS)
	custom_files.UpdateFile("", tftp_hostname, root_fs+"/mount/etc/hostname")

	// cut any path off the filename
	tftp_file_name := filename
	basename := strings.Split(filename, "/")
	if len(basename) > 1 {
		tftp_file_name = basename[1]
	}
	tftp_file := TFTP_BASE + tftp_hostname + "/mount/" + tftp_file_name
	// now to server files

	_, err := os.Open(tftp_file)
	// fmt.Println("Serving file " + tftp_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return err
	}
	file, err := os.Open(tftp_file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return err
	}

	// rf.(tftp.OutgoingTransfer).SetSize(tftp_file)
	n, err := rf.ReadFrom(file)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return err
	}
	fmt.Printf("%d bytes sent\n", n)
	return nil
}

func main() {
	// use nil in place of handler to disable read or write operations
	fmt.Println("newserver")
	s := tftp.NewServer(readHandler, nil)
        fmt.Println("set timeout")
	s.SetTimeout(5 * time.Second)  // optional
        fmt.Println("connect port")
	err := s.ListenAndServe("192.168.1.80:69") // blocks until s.Shutdown() is called
	if err != nil {
		fmt.Fprintf(os.Stdout, "server: %v\n", err)
		os.Exit(1)
	}
}
