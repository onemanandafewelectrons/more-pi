package boot

import (
	"fmt"
	"onemanandafewelectrons/more-pi/overlay"
	"os"
	"os/exec"
)

var NFS_SERVER string

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func create_dir(dir string) {
	_, err := os.ReadDir(dir)
	if err != nil {
		fmt.Println("dir not exist " + dir)
		check(os.MkdirAll(dir, 0755))
	}
}

func cmdline_txt(cmdline_txt_file string, work_dir string, tftp_hostname string, root_fs string) {
	txt := "console=serial0,115200 console=tty1 root=/dev/nfs rootfstype=nfs nfsroot=" + NFS_SERVER + ":" + root_fs + ",vers=3 rw ip=::::" + tftp_hostname + "::dhcp elevator=deadline rootwait cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1"
	cmd_line := "echo " + txt + " > " + cmdline_txt_file
	exec.Command("/bin/bash", "-c", cmd_line).Output()

}

func Create(tftp_hostname string, BASE_PATH string, TFTP_SRC_DIR string, filename string, root_fs string, nfs_server string) string {
	NFS_SERVER = nfs_server
	client_path := BASE_PATH + tftp_hostname + "/"
	upper_dir := client_path + "upper"
	work_dir := client_path + "work"
	mount_dir := client_path + "mount"
	create_dir(upper_dir)
	create_dir(work_dir)
	create_dir(mount_dir)

	if overlay.Check(mount_dir) != 1 {
		mount_options := "lowerdir=" + TFTP_SRC_DIR + ",upperdir=" + upper_dir + ",workdir=" + work_dir + ",nfs_export=on,index=on,redirect_dir=nofollow"
		overlay.Mount(tftp_hostname+"_boot", mount_dir, mount_options)
	}

	cmdline_txt(mount_dir+"/cmdline.txt", work_dir, tftp_hostname, root_fs)
	return mount_dir
}
