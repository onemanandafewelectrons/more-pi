package nfs

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

var MOUNT_OPTIONS = "rw,sync,no_subtree_check,no_root_squash,nohide,crossmnt"
var export_file = "/etc/exports"

// read export file
func readExports() string {

	keys, _ := ioutil.ReadFile(export_file)
	return string(keys)

}

// check if export is in export_File
func is_key_in_file(key string, keys string) bool {
	return strings.Contains(keys, key)

}
func write_exports(content string) {
	err := os.WriteFile(export_file, []byte(content), 0600)

	if err != nil {
		fmt.Println(err)
		return
	}
	// make sure the perms are correct
	err = os.Chmod(export_file, 0600)
	if err != nil {
		log.Fatal(err)
	}
}

func reloadExports() {
	cmd, err := exec.Command("exportfs", "-r").Output()
	if err != nil {
		println(err)
		return
	}
	println(cmd)
}

func AddMount(export string, client string, fsid string) {
	exportfile := readExports()
	NewEntry := export + "        " + client + "(fsid=" + fsid + "," + MOUNT_OPTIONS + ")"
	// for _, line := range strings.Split(exportfile, "\n") {
	// println("line " + line + "   newentry  " + NewEntry)
	key_exists := is_key_in_file(NewEntry, exportfile)
	if !key_exists {
		println("not in file")
		exportfile += "\n" + NewEntry

	}
	if len(exportfile) < 2 {
		exportfile = NewEntry
	}

	// }
	write_exports(exportfile)
	reloadExports()
}
