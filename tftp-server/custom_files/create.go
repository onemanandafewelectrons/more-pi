package custom_files

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

// read file
func readFile(filename string) string {
	content, _ := ioutil.ReadFile(filename)
	return string(content)
}

// check if line is in file
func is_line_in_file(line string, content string) bool {
	return strings.Contains(content, line)
}
func write_file(content string, file string) {
	err := os.WriteFile(file, []byte(content), 0664)

	if err != nil {
		fmt.Println(err)
		return
	}
	// make sure the perms are correct
	err = os.Chmod(file, 0664)
	if err != nil {
		log.Fatal(err)
	}
}

func UpdateFile(content string, line string, filename string) {
	key_exists := is_line_in_file(line, content)
	if !key_exists {
		println("not in file " + line)
		content += "\n" + line

	}
	write_file(content, filename)
}
func CreateFSTAB(root string, docker_mount string, kubelet_mount string, boot string, nfs_server string, boot_scripts string) {

	fstab := root + "/etc/fstab"
	println("upding " + fstab)
	fstab_content := "proc	/proc	proc	defaults 0 0\n"
	fstab_content += nfs_server + ":" + root + "	/ 		nfs4 rw,soft 0 0\n"
	fstab_content += nfs_server + ":" + boot_scripts + "	/boot_scripts 		nfs4 rw,soft 0 0\n"
	fstab_content += nfs_server + ":" + docker_mount + "	/var/lib/docker	 	nfs4 rw,soft 0 0\n"
	fstab_content += nfs_server + ":" + kubelet_mount + "	/var/lib/kubelet	nfs4 rw,soft 0 0\n"
	fstab_content += nfs_server + ":" + boot + " /boot	 	nfs4 rw,soft 0 0\n"
	for _, line := range strings.Split(fstab_content, "\n") {
		old_content := readFile(fstab)

		UpdateFile(old_content, line, fstab)
	}
}
